import React, { Component } from 'react';

import web3 from './contracts/web3';
import contract from './contracts/sampleContract';

import logo from './logo.svg';
import './App.css';


class App extends Component {
	
	state = {
		userBalance: 'DEFAULT_VALUE',
		userAddress: 'DEFAULT_VALUE',
		contractAddress: 'DEFAULT_VALUE',
		contractBalance: 'DEFAULT_VALUE',
		currentId: 'UNDEFINED'
	}
	
	async componentDidMount() {
		const accounts = await web3.eth.getAccounts();
		var userBalance = await web3.eth.getBalance(accounts[0]);
		userBalance = web3.utils.fromWei(userBalance, 'ether');
		
		const contractBalance = await web3.eth.getBalance(contract.options.address);
		
		this.setState({ userAddress: accounts[0] , userBalance: userBalance, contractAddress: contract.options.address, contractBalance: contractBalance});
	}
	
	onGetId = async (event) => {
		event.preventDefault();
		contract.methods.getId().call({from: this.state.userAddress}, (error, result) => {
			if(error){
				console.log(error);
			}else{
				console.log(result);
				this.setState({ currentId: result });
			}
		});
	}
	
	onIncrement = async (event) => {
		event.preventDefault();
		contract.methods.incrementId().send({from: this.state.userAddress}, (error, result) => {
			if(error){
				console.log(error);
			}else{
				console.log(result);
			}
		});
	}
	
	
	render() {
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<p>
						SampleContract - dapp
					</p>
					<div id="box">Contract : {this.state.contractAddress} - {this.state.contractBalance} ETH</div>
					<p id="id">Id : {this.state.currentId}</p>
					<button onClick={this.onGetId}>GetId</button>
					<button onClick={this.onIncrement}>IncrementId</button>
					<div id="h"></div>
				</header>
			</div>
		);
	}
}

export default App;
