# web-dapp-template

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting Started

This repository contains a decentralized web application to interact with a smart contract

### Prerequisites

* Node JS installed
* Metamask (or equivalent) connected to a working network (Ganache for development purpose)
* The ABI and the address of a smart contract deployed on the same network

### Installing

Download dependencies

```
npm install
```

### Running the decentralized web application

```
npm start
```

Then access the default address of the web application

```
http://localhost:3000/
```



